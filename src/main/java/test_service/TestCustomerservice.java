/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author user
 */
public class TestCustomerservice {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer:cs.getCustomers()){
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0868888888"));
        Customer cus1 = new Customer("Kob","0922757567");
        cs.addNew(cus1);
        for(Customer customer:cs.getCustomers()){
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0922757567");
        delCus.setTel("0922757568");
        cs.update(delCus);
        System.out.println("After updated");
        for(Customer customer:cs.getCustomers()){
            System.out.println(customer);
        }
        cs.delete(delCus);
        for(Customer customer:cs.getCustomers()){
            System.out.println(customer);
        }
    }
}
